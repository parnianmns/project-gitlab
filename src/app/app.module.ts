import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AddContactComponent } from './components/add-contact/add-contact.component';
import { HeaderComponent } from './components/header/header.component';




import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatRadioModule} from '@angular/material/radio';
import { RouterModule, Routes } from '@angular/router';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 
import { ListContactComponent } from './components/list-contact/list-contact.component';
import { HttpClientModule } from '@angular/common/http'; 
import { MatSnackBarModule } from '@angular/material/snack-bar';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import { DeleteComponent } from './components/delete/delete.component';
import { EditContactComponent } from './components/edit/edit.component';
import { TestValidatorComponent } from './components/test-validator/test-validator.component';
import { CustomValidatorComponent } from './components/custom-validator/custom-validator.component';
import { PasswordStrengthDirective } from './directives/template-driven.directive';
import { MultiFiledComponent } from './components/multi-filed/multi-filed.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { LoginComponent } from './components/loginUser/login.component';
import { RegisterComponent } from './components/registerUser/register.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { LoginTestComponent } from './components/login-test/login-test.component';
import { RegisterTestComponent } from './components/register-test/register-test.component';






@NgModule({
  declarations: [
    AppComponent,
    AddContactComponent,
    HeaderComponent,
    ListContactComponent,
    DeleteComponent,
    EditContactComponent,
    TestValidatorComponent,
    CustomValidatorComponent,
    PasswordStrengthDirective,
    MultiFiledComponent,
    LoginComponent,
    RegisterComponent,
    NotFoundComponent,
    LoginTestComponent,
    RegisterTestComponent
  ],
  imports: [
    MatIconModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    MatToolbarModule,
    MatButtonModule,
    MatInputModule,
    MatRadioModule,
    MatAutocompleteModule,
    MatSnackBarModule,
    MatTableModule,
    AppRoutingModule,
    MatFormFieldModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
