export interface LoginModel{
    username:string;
    password:string;
}
export interface RegisterModel{
    firstName:string;
    lastName:string;
    username:string;
    password:string;
    confirmPassword:string;
    email:string
}