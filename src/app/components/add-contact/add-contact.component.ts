import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { Router } from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import { ContactService } from 'src/app/services/contact-service.service';
import { ContactModel } from 'src/app/addContact.model';


@Component({
  selector: 'app-add-contact',
  templateUrl: './add-contact.component.html',
  styleUrls: ['./add-contact.component.scss']
})
export class AddContactComponent implements OnInit {

  optionProvince:string[]=['اصفهان','تهران'];
  filterProvince:Observable<string[]> |undefined //chon montazerim ye chizi bargarde az observable estefade mikonim

  form!:FormGroup;
  user!:ContactModel;
  base64Output !: string;



  constructor(private fb:FormBuilder,private router:Router,private _snackBar: MatSnackBar,private _contactService:ContactService) {}

  ngOnInit(): void {
    this.form=this.fb.group({
      name:[null,[Validators.required,Validators.minLength(3)]],
      email:['',{validators:[Validators.required,Validators.email]}],
      family:[null,[Validators.required,Validators.minLength(3)]],
      // email:[null,[Validators.required,Validators.email]],
      phone:[null,[Validators.required,Validators.minLength(9),Validators.pattern(/^\+989\d{9}$/)]],
      address:[null,Validators.required],
      gender:[null,Validators.required],
      province:[null,Validators.required],
      bio:[null,Validators.required],
      upload:[null]
      
      
    })
     this.filterProvince = this.form.get('province')?.valueChanges.pipe(
      startWith(''),
      map((value: any) => this._filter(value))
    );
  }
  
  private _filter(value: string): string[] {
    const filterValue = value ? value.toLowerCase():'';

    return this.optionProvince.filter(option => option.toLowerCase().indexOf(filterValue) === 0);//tabdil mikone be horoofe koochik ke age ba harfe bozorg ham neveshte boodim betoone peida kone
  }

  onSubmit(){
    // this.user=Object.assign({},this.form.value)
    this.user=this.form.value;
    if(this.form.invalid){
      
      this._snackBar.open('تمام فیلد های مربوطه را پر کنید', ' ', {
        duration: 4000,
        horizontalPosition: 'center',
        verticalPosition: 'top',
        
      });
      
      }else{
        this.user.id=Math.floor(Math.random()*10000);
        this.user.avatar=this.base64Output;
        this._contactService.add(this.user).subscribe(response=>{
          this.form.reset();
          this.router.navigate(['/listContact']);
        })
      }
      
  
  }
  onFileSelected(event:any) {
    this._contactService.convertFile(event.target.files[0]).subscribe(base64 => {
      this.base64Output = "data:image/png;base64,"+base64;
    });
  }

}


//phone number regEx

// export function validator():ValidatorFn{
//   return (control:AbstractControl) : ValidationErrors | null=>{
//     const value = control.value;

//     if(!value){
//       return null;
//     }
//     const phoneNumberValidation='^\+989\d{9}$';
//     return !phoneNumberValidation?{phoneValidator:true}:null;
//   }
// }


