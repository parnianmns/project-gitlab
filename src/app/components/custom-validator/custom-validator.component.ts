import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators,ValidatorFn,AbstractControl,ValidationErrors } from '@angular/forms';
import { createPasswordStrengthValidator } from 'src/app/directives/template-driven.directive';

@Component({
  selector: 'app-custom-validator',
  templateUrl: './custom-validator.component.html',
  styleUrls: ['./custom-validator.component.scss']
})


//custom validator baraie form haie reactive form hastesh yani unaii ke formBuiler daran

export class CustomValidatorComponent implements OnInit {

  //optinal:
      // validators: contains the list of synchronous validators for the form field
      // asyncValidators: contains the list asynchronous validators for the form field (not used in this example)
      // updateOn: specifies the moment when the form should be updated with the new form fiel


  newform=this.fb.group({
   // the email field is marked as mandatory via the Validators.required validator, and it also needs to follow the format of a valid email, due to the use of the Validators.email validator.
   //pas ejbari boodane email az tarighe Validators.required , motabar boodane emial az tarighe Validators.email 
    email:['',[Validators.required,emailValidationFunction()]],
    //updateOn='blur','change','submit'
    password:['',[Validators.required,Validators.minLength(8)]],
    //Once we have a new custom validator function written, all we have to do is plug it in our reactive form:
  })

  //Let's remember, the createPasswordStrengthValidator() function did not need input arguments, but we could have defined any arguments that we needed in order to perform the validation, and pass them in this function call.


  //we use formBuilder api to define our form fields and validation rules

  constructor(private fb:FormBuilder) { 

  }

  ngOnInit(): void {
  }
  get email(){
    return this.newform.controls['email']
  }
  get password() {
    return this.newform.controls['password'];
}

}


export function emailValidationFunction(): ValidatorFn {
    return (control:AbstractControl) : ValidationErrors | null => {
        const value = control.value;

        if (!value) {
            return null;
        }
        const emailValid=/[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)/.test(value);
        return !emailValid?{emailValidation:true}:null;

    }
}