import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';

@Component({
  selector: 'app-multi-filed',
  templateUrl: './multi-filed.component.html',
  styleUrls: ['./multi-filed.component.scss']
})
export class MultiFiledComponent implements OnInit {
  multiForm:FormGroup;

  constructor(private fb:FormBuilder) { 

   this.multiForm= this.fb.group(
      {
      startAt:['',Validators.required],
      endAt:['',Validators.required]
    },{
      validators:[creatDateRangeValidator()]
    }
      );
//this.multiForm.setValidators(creatDateRangeValidator());
  }

  ngOnInit(): void {
  }
//   get start(){
//     return this.multiForm.controls['startAt']
//   }
//   get end() {
//     return this.multiForm.controls['endAt'];
// }

}

export function creatDateRangeValidator():ValidatorFn{

 return(control:AbstractControl):ValidationErrors|null=>{
  const start:Date=control.get('startAt')?.value;

   const end:Date=control.get('endAt')?.value;
   if(start&&end){
     const range=(new Date(end).getTime()- new Date(start).getTime()>0);
     return range?null:{dateRange:true}
   }
   return null;
 }

}