import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserService } from 'src/app/services/user.service';
import {  RegisterModel } from 'src/app/user.model';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup
  constructor(private fb: FormBuilder, private router: Router, private _snackBar: MatSnackBar, private _userService: UserService) {
    this.registerForm = this.fb.group({
      lastName: [null, Validators.required],
      firstName: [null, Validators.required],
      username: [null, Validators.required],
      email:[null,[Validators.required,emailVlidate()]],
      password: [null, [Validators.minLength(6), Validators.required]],
      confirmPassword: [null, [Validators.required]]
    }, { validators: this.checkPassword }
    );
  }

  ngOnInit(): void {

  }
  onSubmit() {
    if (this.registerForm.invalid) {

      this._snackBar.open('', 'تمام فیلد های مربوطه را پر کنید', {
        horizontalPosition: "center",
        verticalPosition: "top",
        duration: 4000
      });

    } else {
      let user: RegisterModel = this.registerForm.value;
      this._userService.registerUser(user).subscribe(response => {
        this._snackBar.open('ثبت نام شما با موفقیت انجام شد.');
        this.router.navigate(['/login']);
      })

    }
  }
  checkPassword(registerForm: FormGroup) {
    const pass = registerForm.get('password')?.value;
    const confirm = registerForm.get('confirmPassword')?.value;
    return pass === confirm ? null : { notSame: true }
  }
 
}
export function emailVlidate():ValidatorFn{
  return (control:AbstractControl):ValidationErrors|null=>{
    const value = control.value;

    if(!value){
      return null
    }
    const validate=/[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)/.test(value);
    return !validate?{emailIsValid:true}:null

  }
}

