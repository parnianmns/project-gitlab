import { UpperCasePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';

@Component({
  selector: 'app-test-validator',
  templateUrl: './test-validator.component.html',
  styleUrls: ['./test-validator.component.scss']
})
export class TestValidatorComponent implements OnInit {


  form: FormGroup;
  constructor(
    private fb: FormBuilder
  ) {
    this.form = this.fb.group({
      name: ['', [CustomValidator()]]
    });
  }

  ngOnInit(): void {
  }

}




export function CustomValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null=>{
    const value = control.value;

    if (!value) {
      return null;
    }

    const hasUpperCase = /[A-Z]+/.test(value);

    if (!hasUpperCase)
      return { upper: true };
    else
      return null;
  }
}