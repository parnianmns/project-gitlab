import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { Router } from '@angular/router';
import { element } from 'protractor';
import { ContactModel } from 'src/app/addContact.model';
import { ContactService } from 'src/app/services/contact-service.service';


@Component({
  selector: 'app-list-contact',
  templateUrl: './list-contact.component.html',
  styleUrls: ['./list-contact.component.scss']
})
export class ListContactComponent implements OnInit {


 

  displayedColumns: string[] = ['avatar','name','lastName','email','phoneNubmer','province','address','bio','upload','gender','edit','delete','star'];

  dataSource =new  MatTableDataSource<ContactModel>();

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(private _contactService:ContactService,private router:Router) { }

  ngOnInit(): void {
    this.showlists();
  }

  showlists(){
    this._contactService.show().subscribe((response:ContactModel[])=>{
      this.dataSource.data=response;
    })
  }
  deleteRow(id: number){
    this._contactService.delete(id).subscribe(response=>{
      alert('حذف با موفقیت انجام شد')
        this.showlists();
    })
  }
 
  // edit(ele:ContactModel){
  //   this._contactService.

  // }

 changeColor(el:ContactModel){
   this._contactService.starColor(el.id).subscribe(response=>{
    el.star=!el.star;
   })
 }
}



