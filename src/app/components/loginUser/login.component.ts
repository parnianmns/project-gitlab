import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { LoginModel } from 'src/app/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm!:FormGroup;
  user!:LoginModel;
  constructor(private fb:FormBuilder,private ActivatedRoute:ActivatedRoute,private _userService:UserService,private router:Router,private _snackBar:MatSnackBar) { }

  ngOnInit(): void {
    this.loginForm=this.fb.group({
      password:[null,[Validators.required,Validators.minLength(6)]],
      username:[null,Validators.required]
    })
  }

  onSubmit(){
    this.user=this.loginForm.value;

    if(this.loginForm.invalid){

      this._snackBar.open('', 'تمام فیلد های مربوطه را پر کنید', {
        horizontalPosition: "center",
        verticalPosition: "top",
        duration:4000
      });


    }else{



      this._userService.loginUser(this.user).subscribe(response=>{
        if(response==true)
        this.router.navigate(['/listContact']);
        else{
          this._snackBar.open('نام کاربری یا کلمه عبور صحیح نیست!');
        }
      })
   
    }
  }


}

