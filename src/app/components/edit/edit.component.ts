
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ContactService } from 'src/app/services/contact-service.service';
import { ContactModel } from 'src/app/addContact.model';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditContactComponent implements OnInit {
  contactId!: string;

  optionProvince: string[] = ['اصفهان', 'تهران'];
  filterProvince: Observable<string[]> | undefined //chon montazerim ye chizi bargarde az observable estefade mikonim

  form!: FormGroup;
  user!: ContactModel;
  base64Output!: string;


  constructor(private fb: FormBuilder, private router: Router, private _snackBar: MatSnackBar,
    private _contactService: ContactService, private _activatedRoute: ActivatedRoute) {
    this.form = this.fb.group({
      id:[''],
      name: [null, [Validators.required, Validators.minLength(3)]],
      family: [null, [Validators.required, Validators.minLength(3)]],
      email: [null, [Validators.required, Validators.email]],
      phone: [null, [Validators.required, Validators.minLength(11)]],
      address: [null, Validators.required],
      gender: [null, Validators.required],
      province: [null, Validators.required],
      bio: [null, Validators.required],


    });

    //raveshe aval ba estefade az params
    this._activatedRoute.params.subscribe((p: any) => {
      this.contactId = p.id;
      this.getInfo();
    });

  }

  ngOnInit(): void {

    this.filterProvince = this.form.get('province')?.valueChanges.pipe(
      startWith(''),
      map((value: any) => this._filter(value))
    );
  }

  private _filter(value: string): string[] {
    const filterValue = value ? value.toLowerCase() : '';

    return this.optionProvince.filter(option => option.toLowerCase().indexOf(filterValue) === 0);//tabdil mikone be horoofe koochik ke age ba harfe bozorg ham neveshte boodim betoone peida kone
  }

  onSubmit() {
    // this.user=Object.assign({},this.form.value)
    this.user = this.form.value;
    if (this.form.invalid) {

      this._snackBar.open('تمام فیلد های مربوطه را پر کنید', ' ', {
        duration: 4000,
        horizontalPosition: 'center',
        verticalPosition: 'top',

      });

    } else {
      let data=this.form.value;
      data.avatar=this.base64Output;
      // alert('آفرین ذخیره را خودت بنویس');
      this._contactService.editContact(data).subscribe(response => {
        this._snackBar.open('با موفقیت ذخیره شد.');
        this.router.navigate(['listContact']);

      })

    }


  }

  getInfo() {
    this._contactService.getContactInfo(+this.contactId).subscribe(response => {
      if (response){
        this.form.patchValue(response);
        this.base64Output=response.avatar;
      }else {
        alert('پیدایش نشد!');
        this.router.navigate(['/listContact']);
      }
    })
    // روش دوم با استفاده از paramMap
    // this._activatedRoute.paramMap.subscribe(p=>{
    //   this.contactId=p.get('id')?.valueOf();
    // });

  }

  onFileSelected(event:any) {
    this._contactService.convertFile(event.target.files[0]).subscribe(base64 => {
      this.base64Output = "data:image/png;base64,"+base64;
    });
  }


}




