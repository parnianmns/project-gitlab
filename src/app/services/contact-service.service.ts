
import { Injectable, OnInit } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';
import { ContactModel } from '../addContact.model';

@Injectable({
  providedIn: 'root'
})
export class ContactService implements OnInit {

  localList: any[] = []
  constructor() { }
  ngOnInit() {

  }
  // marhaleie aval ezafe mikonim ba local storage
  //aval az hame baiad bebinim ke aia chizi dare az local storage migire ya na(ba if()):hala age gerefte bood chizi baiad maghadire formemoono too ye liste khali berizim chon mikham too ye list namaieshesh bedim

  add(user: any) {
    return new Observable(observer => {
      if (localStorage.getItem('get')) {
        this.localList = JSON.parse(localStorage.getItem('get') || '[]');
        this.localList = [user, ...this.localList]
      } else {
        this.localList = [user]
      }
      localStorage.setItem('get', JSON.stringify(this.localList));
      observer.next(true)
    })
  }
  //marhaleie dovom neshunesh midim
  show(): Observable<ContactModel[]> {
    return new Observable<ContactModel[]>(observer => {

      this.localList = JSON.parse(localStorage.getItem('get') || '[]');
      observer.next(this.localList)
    })
  }

  //marhaleie hazf ke row
  delete(id: number) {
    return new Observable(o => {
      this.localList = JSON.parse(localStorage.getItem('get') || '[]');
      let deleteList = this.localList.forEach((item: ContactModel, index: number) => {
        if (item.id == id) {
          this.localList.splice(index, 1);
        }
      });

      localStorage.setItem('get', JSON.stringify(this.localList));
      o.next(true);
    })
  }


  /*
ذخیره داده های مخاطب
  */
  editContact(el: ContactModel) {
    return new Observable(obs => {
      this.localList = JSON.parse(localStorage.getItem('get') || '[]');
      let editList = this.localList.map(item => {
        if (item.id == el.id) {
          item = el;
        }
return item;
      });

      localStorage.setItem('get', JSON.stringify(editList));
      obs.next(true);
    })
  }



  /*
  لود داده های یک مخاطب
  */
  getContactInfo(id: number): Observable<ContactModel> {
    var contactItem:any='';
    return new Observable(o => {
      this.localList = JSON.parse(localStorage.getItem('get') || '[]');
      let deleteList = this.localList.forEach((item: ContactModel, index: number) => {
        if (item.id == id) {
          contactItem=item;
        }


      });
          localStorage.setItem('get',JSON.stringify(this.localList));
          o.next(contactItem);

    });

  }

  starColor(id:number){

    return new Observable(observe=>{
      this.localList=JSON.parse(localStorage.getItem('get')||'[]');
      let lists=this.localList.map(item=>{
        if(item.id==id){
          item.star=!item.star;
        }
      
      })
      localStorage.setItem('get', JSON.stringify(this.localList));
      
      observe.next(true);

    })
   
  }
  
  convertFile(file : File) : Observable<string> {
    const result = new ReplaySubject<string>(1);
    const reader = new FileReader();
    reader.readAsBinaryString(file);
    reader.onload = (event:any) => result.next(btoa(event.target.result.toString()));
    return result;
  }

}

  
  