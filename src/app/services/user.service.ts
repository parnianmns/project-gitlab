
import { Injectable } from '@angular/core';
import { checkServerIdentity } from 'node:tls';
import { observable, Observable } from 'rxjs';
import { LoginModel, RegisterModel } from '../user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor() { }
  

  //zakhireie etelaat dar localStorage
  //too marhale aval baiad bebinim chizi az local storage khunde ya na
 
  registerUser(user: RegisterModel) {
    return new Observable(observer => {
      let registerList:any[]=[];
      if (localStorage.getItem('user')) {

      registerList = JSON.parse(localStorage.getItem('user') || '[]');
        registerList = [user, ...registerList]
      } else {
        registerList = [user]
      }
      localStorage.setItem('user', JSON.stringify(registerList));
      observer.next(true)
    })
  }

  loginUser(user:LoginModel){
    return new Observable(observer=>{
      let registerList:any[]=[];
      let userFinded=false;
      registerList=JSON.parse(localStorage.getItem('user')||'[]');
    let checkList=registerList.forEach((element,index)=> {
       if(element.username == user.username  &&  element.password==user.password){
         userFinded=true;
       }
     });
      return observer.next(userFinded);
    })
  }
}

