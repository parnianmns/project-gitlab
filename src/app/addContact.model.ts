
export interface ContactModel{
    id:number;
    name:string;
    lastName:string;
    email:string;
    phoneNumber:number;
    address:string;
    province:string;
    city:string;
    gender:string;
    bio:string;
    avatar:string;
    star:boolean;
}

export enum Gender{
    man,woman
}

export interface ProvinceModel{
    id:number;
    title:string;

}

export interface CityModel{
    
    parentId:number;
    id:number;
    title:string;
}