import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddContactComponent } from './components/add-contact/add-contact.component';
import { CustomValidatorComponent } from './components/custom-validator/custom-validator.component';
import { EditContactComponent } from './components/edit/edit.component';
import { HeaderComponent } from './components/header/header.component';
import { ListContactComponent } from './components/list-contact/list-contact.component';
import { LoginTestComponent } from './components/login-test/login-test.component';
import { LoginComponent } from './components/loginUser/login.component';
import { MultiFiledComponent } from './components/multi-filed/multi-filed.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { RegisterComponent } from './components/registerUser/register.component';
import { TestValidatorComponent } from './components/test-validator/test-validator.component';

const routes: Routes = [

  {path:'',component:LoginComponent}, 
  {path:'addContact',component:AddContactComponent},
  {path:'listContact',component:ListContactComponent},
  {path:'edit-contact/:id',component:EditContactComponent},
  { path:'test-validator',component:TestValidatorComponent},
  {path:'custom-validator',component:CustomValidatorComponent},
  {path:'multi',component:MultiFiledComponent},
  {path:'login',component:LoginComponent},
  {path:'register',component:RegisterComponent},
  {path:'login-test',component:LoginTestComponent},
  {path:'404',component:NotFoundComponent},
  {path:'**',redirectTo:'/404'}
  

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
