// compaire custom validation in reactive-forms)(فرم با formBuilder ) vs template-driven(فرم معمولی با ngModel)
//With reactive forms, all we have to do is to write a function. But in template-driven forms, besides implementing a Validator function, we also have to know how to write our own custom directive and know how to configure the dependency injection system.

import { Directive } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn  } from '@angular/forms';

@Directive({
  selector: '[appTemplateDriven]',
  providers:[{

    //The Angular Forms module needs to know what are the multiple custom validation directives available. It does so by asking the dependency injection system for the NG_VALIDATORS injection token (a sort of unique dependency injection key, that uniquely identifies a dependency).
    //yani darhaghighat angular form module ma az koja mifahme ma darim az custom validation directive estefade mikonim? az tarighr providers:NG-VALIDATION ke depedency injection ast
    provide:NG_VALIDATORS,
    //We can see in our directive declaration that we are creating a new provider, meaning that we are setting up a new value in the DI system and we are making the PasswordStrengthDirective available to be instantiated by the DI system if needed
    useExisting:PasswordStrengthDirective,
    //This dependency in of type multi:true meaning that it includes multiple values, and not just one value. This is to be expected, as there are many custom validator directives, and not just one.
    //yani ma mitoonim chandta comtum validation directive dashte bashim pas multi:true 
    //Because the NG_VALIDATORS provider is of type multi:true, we are adding the
    //PasswordStrengthDirective to the list of existing custom validation directives, instead of replacing the new list with a single value.
    multi:true
  }]
  //Notice that adding the proper DI configuration to your custom validation directive is essential for it to work. Without this, the validate() method simply won't get triggered.
})
export class PasswordStrengthDirective implements Validator{
 

  validate(control: AbstractControl):ValidationErrors|null{
    return createPasswordStrengthValidator()(control);
  }

}



//createPasswordStrengthValidator():khodesh tabe etebar sanj nis balke tabe etebar sanj ro ijad mikone
//mitoone argumane voroodi dashte bashe
//khode tabe etebar sanjj farakhuni mishe ta moshakhas beshe form ma motabare yana
export function createPasswordStrengthValidator():ValidatorFn{

  //only one input argument is expected, which is of type AbstractControl. The validator function can obtain the value to be validated  via the control.value property
  //The validator function needs to return null if no errors were found in the field value, meaning that the value is valid
  //The value of the ValidationErrors object can be an object with any properties that we want, allowing us to provide a lot of useful information about the error if needed
 // If we just want to indicate that an error occurred, without providing further details, we can simply return true as the value of an error property in the ValidationErrors object
  return (control:AbstractControl):ValidationErrors|null=>{
    // returning null if the password is valid and no errors were found
    // returning {passwordStrength:true} as an output error object, in case that the password is not strong enough


    const value=control.value;
    //in yani age value khali bood errori neshun nade(null yani errori ndaraim)
    if(!value){
      return  null;
    }
    //in paiinia darhaghighat else hashan
    const hasUpperCase=/[A-Z]+/.test(value);
    const hasLowerCase = /[a-z]+/.test(value);
    const hasNumeric = /[0-9]+/.test(value);
    const passWordValid=hasUpperCase && hasLowerCase && hasNumeric;
    return !passWordValid?{passwordStrength:true}:null;
   
  }
}

